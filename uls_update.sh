#!/bin/bash

# Info
#
# before running this script, create a database named ULSDATA on your mysql host.
#
# edit your .my.cnf to include your mysql host, username, password (see below)
#
# [client]
host="%server%"
user="%user%"
password="%password%"
#

yesterday=$(date -d "1 day ago" +%a)
day=${yesterday,,}

#### Get new FCC data
echo "Getting yesterday's data from FCC ULS"
wget -q ftp://wirelessftp.fcc.gov/pub/uls/daily/l_am_"$day".zip

echo "expanding data"
unzip l_am_"$day".zip
chmod 755 *.dat

#### import the new data
echo "converting shit data to sql"
php work.php
rm EN.dat
mv OUT.dat EN.dat

echo "importing new data"

mysql --user="${user}" --password="${password}" --host="${host}" ULSDATA < load.sql
echo "... done"

echo "done importing this batch"

#### Remove old FCC data files.
echo "cleaning up"
rm -f l_am_"$day".zip *.dat counts
echo "files removed"
