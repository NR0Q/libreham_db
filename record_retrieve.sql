-- SELECT from ULS data, a callsign return single best result

SELECT
    AM.callsign,
    EAT.app_type_name,
    AOC.class_name,
    HDL.status_name,
    HD.radio_service_code,
    EN.last_name,
    EN.first_name,
    EN.mi,
    EN.suffix,
    EN.attention_line,
    EN.po_box,
    EN.street_address,
    EN.city,
    EN.state,
    EN.zip_code,
    HD.grant_date
FROM
    PUBACC_AM AS AM
INNER JOIN PUBACC_EN AS EN ON AM.unique_system_identifier=EN.unique_system_identifier
INNER JOIN PUBACC_HD AS HD ON AM.unique_system_identifier=HD.unique_system_identifier
INNER JOIN EN_ApplicantType AS EAT ON EN.applicant_type_code=EAT.applicant_type_code
INNER JOIN AM_OperatorClass AS AOC ON AM.operator_class=AOC.operator_class
INNER JOIN HD_LicenseStatus AS HDL ON HD.license_status=HDL.license_status
WHERE
    AM.callsign = ?
ORDER BY
    HD.license_status ASC
LIMIT 1;

-- SELECT FROM hamdb data, a callsign single result

SELECT
    callsign,
    last_name,
    first_name,
    mi,
    nick_name,
    suffix,
    po_box,
    street_address,
    city,
    county,
    state,
    postal_code,
    country,
    gridsquare,
    is_uls,
    hide_pobox,
    hide_address,
    hide_city,
    hide_county,
    hide_state,
    hide_postal,
    hide_gridsquare
FROM
    callsigns
WHERE
    callsign = ?
LIMIT 1;

-- SELECT from ULS data, a list from city matching

SELECT
    AM.callsign,
    EAT.app_type_name,
    AOC.class_name,
    HDL.status_name,
    HD.radio_service_code,
    EN.last_name,
    EN.first_name,
    EN.mi,
    EN.suffix,
    EN.attention_line,
    EN.po_box,
    EN.street_address,
    EN.city,
    EN.state,
    EN.zip_code,
    HD.grant_date
FROM
    PUBACC_EN AS EN
INNER JOIN PUBACC_AM AS AM ON EN.unique_system_identifier=AM.unique_system_identifier
INNER JOIN PUBACC_HD AS HD ON PN.unique_system_identifier=HD.unique_system_identifier
INNER JOIN EN_ApplicantType AS EAT ON EN.applicant_type_code=EAT.applicant_type_code
INNER JOIN AM_OperatorClass AS AOC ON AM.operator_class=AOC.operator_class
INNER JOIN HD_LicenseStatus AS HDL ON HD.license_status=HDL.license_status
WHERE
    EN.city LIKE ?
ORDER BY
    AM.callsign ASC;

-- SELECT from hamdb, a list from city matching

SELECT
    callsign,
    last_name,
    first_name,
    mi,
    nick_name,
    suffix,
    po_box,
    street_address,
    city,
    county,
    state,
    postal_code,
    country,
    gridsquare,
    is_uls,
    hide_pobox,
    hide_address,
    hide_city,
    hide_county,
    hide_state,
    hide_postal,
    hide_gridsquare
FROM
    callsigns
WHERE
    city LIKE ?
ORDER BY
    callsign ASC;

-- SELECT from ULS data, a list from name matching

SELECT
    AM.callsign,
    EAT.app_type_name,
    AOC.class_name,
    HDL.status_name,
    HD.radio_service_code,
    EN.last_name,
    EN.first_name,
    EN.mi,
    EN.suffix,
    EN.attention_line,
    EN.po_box,
    EN.street_address,
    EN.city,
    EN.state,
    EN.zip_code,
    HD.grant_date
FROM
    PUBACC_EN AS EN
    INNER JOIN PUBACC_AM AS AM ON EN.unique_system_identifier = AM.unique_system_identifier
    INNER JOIN PUBACC_HD AS HD ON EN.unique_system_identifier = HD.unique_system_identifier
    INNER JOIN EN_ApplicantType AS EAT ON EN.applicant_type_code = EAT.applicant_type_code
    INNER JOIN AM_OperatorClass AS AOC ON AM.operator_class = AOC.operator_class
    INNER JOIN HD_LicenseStatus AS HDL ON HD.license_status = HDL.license_status
WHERE
    EN.last_name LIKE ?
    OR EN.first_name LIKE ?
ORDER BY
    callsign ASC;

-- SELECT from hamdb, a list from name matching

SELECT
    callsign,
    last_name,
    first_name,
    mi,
    nick_name,
    suffix,
    po_box,
    street_address,
    city, county,
    state,
    postal_code,
    country,
    gridsquare,
    is_uls,
    hide_pobox,
    hide_address,
    hide_city,
    hide_county,
    hide_state,
    hide_postal,
    hide_gridsquare
FROM
    callsigns
WHERE
    last_name LIKE ?
    OR first_name LIKE ?
ORDER BY
    callsign ASC;
    