FROM debian:stable

WORKDIR /home/
ADD * /home/
RUN touch /home/auth.json
RUN apt-get update && apt-get install -yqq cron curl gnupg php php-mysql wget unzip

ADD my-cron /etc/cron.d/my-cron
RUN chmod 0644 /etc/cron.d/my-cron && crontab /etc/cron.d/my-cron && touch /var/log/uls.log
CMD tail -f /var/log/uls.log
